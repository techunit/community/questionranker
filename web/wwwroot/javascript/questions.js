// commentaire de test
// commentaire 3

var list_questions_chrono;
var list_questions_rank;

/**
Fonctions pour gérer l'affichage du jour / mois si nécessaire dans l'UI. 
**/
/*
function getDayStr(dayIndex) 
{
	return ["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"][dayIndex] || '';
}

function getMonthStr(dayIndex) 
{
	return ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"][dayIndex] || '';
}
*/


/****
* load_question
* no @arg
* -----------
* Interroge le serveur pour obtenir la liste des questions et l'affichée.
* Est appelée au chargement de la page.
* -----------
* TODO :
*	- Ajouter la récupération des données triées par « RANK »
****/
function load_questions() 
{
	var xhr = new XMLHttpRequest();


// rappel : le code doit être géré dans le onreadystatechange pour ne pas être désynchronisé
	xhr.onreadystatechange = function() {
		if (xhr.readyState == XMLHttpRequest.DONE) {
			list_questions_chrono = JSON.parse(xhr.responseText);
			list_questions_rank = JSON.parse(xhr.responseText);
			console.debug(list_questions_chrono);

			document.getElementById("liste_question_chrono").innerHTML = "";
			document.getElementById("liste_question_rank").innerHTML = "";

			add_questions_chrono(list_questions_chrono);
			add_questions_rank(list_questions_rank);

			// on se met à la fin des questions dans la liste de gauche « chrono », on ne touche pas à la liste de droite « rank »
			document.getElementById("liste_question_chrono").scrollTop = document.getElementById("liste_question_chrono").scrollHeight;
		}
	}
	xhr.open('GET', 'https://aftertbot004.azurewebsites.net/api/question', true);
	xhr.send();
}


/****
* get_last_question
* no @arg
* -----------
* Interroge le serveur pour obtenir la liste des questions depuis la dernière date connue.
* Est appelée régulièrement par un cron pour mettre à jour les questions à l'utilisateur
* -----------
* TODO :
*	- Rajouter l'appel de cette fonction dans le cron de index.html
*	- Vérifier que le serveur renvoie aucune question (que le serveur fasse bien > à la date et non >=)
****/
function get_last_questions() 
{
	// En 3 temps : 

	var query_date;
	var question_list;
	var xhr_user = new XMLHttpRequest();

	query_date = list_questions_chrono[list_questions_chrono.length - 1].date
	console.debug(query_date);
	query_date = query_date.replace(":","%3A");
	query_date = query_date.replace("+","%2B");
	var url= "https://aftertbot004.azurewebsites.net/api/question" + "?date=" + query_date;
	console.debug(url);

	var xhr = new XMLHttpRequest();

	xhr.onreadystatechange = function() {
		if (xhr.readyState == XMLHttpRequest.DONE) {
			console.debug(xhr.responseText);
			if(xhr.responseText != "") {
				question_list = JSON.parse(xhr.responseText);
				console.debug("question_list 1");
				console.debug(question_list);

				list_questions_chrono.push.apply(list_questions_chrono, question_list);
				list_questions_rank.push.apply(list_questions_rank, question_list);
				console.debug("Push questions : ");
				console.debug(list_questions_chrono);
				console.debug(list_questions_rank);

				add_questions_chrono(question_list);

				// on se met à la fin des questions dans la liste de gauche « chrono », on ne touche pas à la liste de droite « rank »
				document.getElementById("liste_question_chrono").scrollTop = document.getElementById("liste_question_chrono").scrollHeight;
			}
		}
	}
	
	xhr.open("GET", url, true);
	xhr.setRequestHeader('Content-Type', 'application/json');

	xhr.send(null);
}


/****
* add_questions_chrono
* @arg1 : question list ==> Collection de tableau associatif des questions
* -----------
* Ajoute à la fin de la section de gauche « chrono » les questions passées en paramètres.
* -----------
* TODO :
*	- Améliorer l'UI
*	- Ajouter la date et l'heure dans l'UI ?
****/
function add_questions_chrono(question_list) 
{
	var date = "";
	var fdate = "";
	for(i=0 ; i<question_list.length ; i++) {
		date = new Date(question_list[i].date);
		fdate = "à " + (date.getHours()<10?'0':'') + date.getHours() + ":" + (date.getMinutes()<10?'0':'') + date.getMinutes() + ":" + (date.getSeconds()<10?'0':'') + date.getSeconds();
document.getElementById("liste_question_chrono").innerHTML += `
	<div class="question_container" id="chrono_` + question_list[i].id + `">
		<div class="auth_quest_container">
			<div class="author" id="auth_chrono_` + question_list[i].id + `">` + question_list[i].authorId + `</div>
			<div class="date" id="date_chrono_` + question_list[i].id+ `">` + fdate + `</div>
			<div class="hidden_date" id="hiddendate_chrono_` + question_list[i].id+ `">` + question_list[i].date + `</div>
			<div class="question" id="question_chrono_` + question_list[i].id+ `">` + question_list[i].content + `</div>
		</div>
		<div class="action_container">
			<div class="action_count_container" onClick="Javascript:vote('`+question_list[i].id+`', '` + question_list[i].authorId + `', 2);">
				<div id="up_chrono_` + question_list[i].id + `" class="nbup">` + question_list[i].nbOk + `</div>
				<div class="up">+</div>
			</div>
			<div class="action_count_container" onClick="Javascript:vote('`+question_list[i].id+`', '` + question_list[i].authorId + `', 1);">
				<div id="hs_chrono_` + question_list[i].id + `" class="nbhs">` + question_list[i].nbOff + `</div>
				<div class="hs">HS</div>
			</div>
			<div class="action_count_container" onClick="Javascript:vote('`+question_list[i].id+`', '` + question_list[i].authorId + `', 0);">
				<div id="down_chrono_` + question_list[i].id + `" class="nbdown">` + question_list[i].nbKo + `</div>
				<div class="down">-</div>
			</div>
		</div>
	</div>`;
	}

	document.getElementById("liste_question_chrono").scrollTop = document.getElementById("liste_question_chrono").scrollHeight;
}

/****
* add_questions_rank
* @arg1 : question list ==> Collection de tableau associatif des questions
* -----------
* Ajoute à la section de droite « rank » les questions passées en paramètres.
* -----------
* TODO :
*	- Améliorer l'UI
*	- Ajouter la date et l'heure dans l'UI ?
*	- détecter la position exacte où insérer l'élément
****/
function add_questions_rank(question_list) 
{
	var date = "";
	var fdate = "";
	for(i=0 ; i<question_list.length ; i++) {
		date = new Date(question_list[i].date);
		fdate = "à " + (date.getHours()<10?'0':'') + date.getHours() + ":" + (date.getMinutes()<10?'0':'') + date.getMinutes() + ":" + (date.getSeconds()<10?'0':'') + date.getSeconds();

document.getElementById("liste_question_rank").innerHTML += `
	<div class="question_container" id="rank_` + question_list[i].id + `">
		<div class="auth_quest_container">
			<div class="author" id="auth_rank_` + question_list[i].id + `">` + question_list[i].authorId + `</div>
			<div class="date" id="date_rank_` + question_list[i].id+ `">` + fdate + `</div>
			<div class="hidden_date" id="hiddendate_rank_` + question_list[i].id+ `">` + question_list[i].date + `</div>
			<div class="question" id="question_rank_` + question_list[i].id+ `">` + question_list[i].content + `</div>
		</div>
		<div class="action_container">
			<div class="action_count_container">
				<div id="up_rank_` + question_list[i].id + `" class="nbup">` + question_list[i].nbOk + `</div>
				<div class="up" onClick="Javascript:vote('`+question_list[i].id+`', '` + question_list[i].authorId + `', 2);">+</div>
			</div>
			<div class="action_count_container">
				<div id="hs_rank_` + question_list[i].id + `" class="nbhs">` + question_list[i].nbOff + `</div>
				<div class="hs" onClick="Javascript:vote('`+question_list[i].id+`', '` + question_list[i].authorId + `', 1);">HS</div>
			</div>
			<div class="action_count_container">
				<div id="down_rank_` + question_list[i].id + `" class="nbdown">` + question_list[i].nbKo + `</div>
				<div class="down" onClick="Javascript:vote('`+question_list[i].id+`', '` + question_list[i].authorId + `', 0);">-</div>
			</div>
		</div>
	</div>`;
	}

	// document.getElementById("liste_question_rank").scrollTop = document.getElementById("liste_question_rank").scrollHeight;
}


/**
* update_question
* @arg1 : question_id ==> string identifiant de la question concernée par le vote
* -----------
* À partir d'un identifiant, met à jour les données 
* -----------
**/
function update_question(question_id) 
{
	console.debug(question_id);
}




/**
	Signfication des indices pour voter :
		indice 0 : up;
		indice 1 : HS;
		indice 2 : down;
**/

/**
* vote
* @arg1 : question_id ==> string identifiant de la question concernée par le vote
* @arg2 : author_id ==> string identifiant de l'utilisateur ayant posé la question
* @arg3 : type ==> integer indice correspondant au type de vote
* -----------
* Envoie le signal de vote au serveur, même code envoyé
* -----------
* TODO :
*	VOTING SYSTEM TODO :
*		FRONT : 
*			- Retirer le author_id une fois que le serveur gèrera correctement le vote
*
*		Le serveur devrait juste recevoir l'indice et gérer le vote :
*			Si jamais voté à la question : on attribue le vote [FAIT]
*			Si déjà voté, on retire le vote	[TODO côté backend]
*			Si c'est un vote pour autre chose, on retire le vote initial pour le nouveau [FAIT]
**/
function vote(question_id, author_id, type) 
{
	console.debug(question_id);
	console.debug(author_id);
	console.debug(type);

	var xhr = new XMLHttpRequest();
	xhr.open("PUT", "https://aftertbot004.azurewebsites.net/api/poll", true);

	xhr.onreadystatechange = function() {
		if (xhr.readyState == XMLHttpRequest.DONE) {
			//console.debug("data sent.");
			//console.debug(xhr);
		}
	}
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify({authorId: author_id, questionId: question_id, indice: type}));
}



/**
* send_question
* @arg1 : peudo ==> string pseudo de l'auteur
* @arg2 : question ==> string question à envoyer
* -----------
* Récupération, vérification et envoi de la question au serveur
* -----------
* TODO :
*	- rajouter la vérification que les champs soient bien remplis
*	- rajouter une vérification sur la taille de la question 
*	- rajouter la question sans avoir à recharger la page entière : 
		- intégrer la question directement à l'UI sans passer par la base
		- ignorer la date de la dite question pour la prochaine MAJ des questions (probablement à gérer ailleurs)
**/
function send_question(pseudo, question)
{
	console.debug("func send_question START");
	console.debug(pseudo.value);
	console.debug(question.value);

	var xhr = new XMLHttpRequest();
	xhr.open("PUT", "https://aftertbot004.azurewebsites.net/api/question", true);

	xhr.onreadystatechange = function() {
		if (xhr.readyState == XMLHttpRequest.DONE) {
			console.debug("question ready & sent");
			pseudo.value = "";
			question.value = "";
			get_last_questions();
		}
	}
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.send(JSON.stringify({authorId: pseudo.value, content: question.value }));
}

