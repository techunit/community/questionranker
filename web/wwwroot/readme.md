### ETAT DU FRONT : *DEV IN PROGRESS*
------------

> Comment tester l'interface et les fonctionnalités ? 

Télécharger le repos ou faire un git clone, puis ouvrir le fichier index.html dans le navigateur
A vos risques et péril, ma version de dev est dispo [à cet endroit](https://patoeuf.fr/thinker_vote/questionranker/web/wwwroot/ "à cet endroit"), mais l'interface peut être cassée car des développement sont en cours



> La page est vide ? Pourquoi il n'y a pas de questions ? 

Parce que le serveur ne possède tout simplement aucune questions à renvoyer. 
Pour effectuer des tests, il faut d'abord envoyer quelques questions avec les champs en bas de l'écran.

> Qu'est ce qui fonctionne ? 

- Au chargement, la page va chercher toutes les données pour les questions en ordre chronologique
- Le système de vote « fonctionne », mais nécessite de rafraichir la page entière pour que la modification soit visible (attention aussi à la potentielle latence du serveur)
- Le formulaire d'envoie d'une question fonctionne également (attention à bien remplir tous les champs)

> Qu'est ce qui ne fonctionne pas / reste à faire ?

- La page ne met pas à jour les données automatiquement, que ça soit les nouvelles questions ou les votes
- L'UI ne gère pas proprement les interactions de l'utilisateurs (rechargement de la page entière, aucun changement visible lors d'une action...)
- Rien n'est géré côté « rank », tout reste à faire
- La date et heure de la question devrait probablement être affichée dans l'UI
- L'interface devrait subir un petit lifting pour être plus attractive
- Gérer l'UI en dessous une certaine largeur d'écran :
 - Affichage d'une seule des deux section sur toute la largeur
 - Ajout de deux onglets en haut, pour chosir par ordre chronologique ou rank 
 - Réorganisation des boutons de vote pour être accessible facilement au tactile


> Quelles autres fonctionnalités sont prévuées ?

- Editer sa question ? 
- Supprimer sa question ? 

